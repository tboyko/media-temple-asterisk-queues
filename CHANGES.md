1.3.2
-----
* Added protection for dead actors after waiting for connect

1.1.18
------
* Added crash protection during execution of spinup callback

1.1.17
------
* Removed keepalive mechanism
* Added additional logging and better error logging

1.0.7
-----
* Increased debug output

1.0.6
-----
* Added support for QueueCallerAbandon event

1.0.5
-----
* Abstracted new store state calls to memory_store implementation

1.0.4
-----
* Callback handler now waits until spinup has completed

1.0.3
-----
* Added spinup callback support

1.0.2
-----
* Added callback support for 'join' action

1.0.1
-----
* Quieted (un)pause logging

1.0.0
-----
* Initial release in a production setting