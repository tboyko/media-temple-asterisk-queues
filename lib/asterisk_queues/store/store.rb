module AsteriskQueues
  module Store
    module Store

      def initialize
      end

      def reset
        raise NotImplementedError
      end

      # Ready State

      def ready!
        @ready = true
      end

      def not_ready!
        @ready = false
      end

      def ready?
        @ready
      end

      # Queues

      def create_queue(queue_name)
        raise NotImplementedError
      end

      def find_queue(queue_name)
        raise NotImplementedError
      end

      # Agents

      def create_agent(agent_name)
        raise NotImplementedError
      end

      def find_agent(agent_name)
        raise NotImplementedError
      end

      def remove_agent(agent_name)
        raise NotImplementedError
      end

      # Assignments

      def create_assignment(queue_name, agent_name)
        raise NotImplementedError 
      end

      def find_assignment(queue_name, agent_name)
        # If no assignment is found, this method should return nil, not raise an error.
        raise NotImplementedError
      end

      def delete_assignment(queue_name, agent_name)
        raise NotImplementedError
      end

      # Callers

      def create_caller(unique_id, queue_name)
        raise NotImplementedError
      end

      def find_caller(unique_id)
        raise NotImplementedError
      end

      def delete_caller(unique_id)
        raise NotImplementedError
      end

    end
  end
end
