module AsteriskQueues
  module Store
    module Caller
      
      # attr_accessors
      %w{channel caller_id_num caller_id_name joined_at answered_at}.each do |method|
        define_method(method) { raise NotImplementedError }
        define_method("#{method}=") { |_v| raise NotImplementedError }
      end

      def set_agent(agent_name)
        raise NotImplementedError
      end

      def save
      end
      
    end
  end
end
