module AsteriskQueues
  module Store
    module Assignment 

      STATUS = {  1 => :not_in_use,
                  2 => :in_use,
                  3 => :busy,
                  4 => :invalid,
                  5 => :unavailable,
                  6 => :ringing,
                  7 => :ring_in_use,
                  8 => :on_hold }

      # attr_accessors
      %w{last_call status paused}.each do |method|
        define_method(method) { raise NotImplementedError }
        define_method("#{method}=") { |_v| raise NotImplementedError }
      end

      def save
      end

    end
  end
end
