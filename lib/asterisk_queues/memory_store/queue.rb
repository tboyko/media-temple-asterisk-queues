module AsteriskQueues
  module MemoryStore
    class Queue

      attr_reader :name, :estimated_hold_time

      def initialize(store, name)
        @name   = name
        @store  = store
      end

      def assignments
        @store.assignments.values.select { |a| a.queue.equal? self }
      end

      def callers
        @store.callers.values.select { |c| c.queue.equal? self }
      end

      def save
      end

    end
  end
end