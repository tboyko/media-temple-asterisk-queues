module AsteriskQueues
	module MemoryStore
  	class Store 
      include AsteriskQueues::Store::Store
    		
      attr_reader :queues, :agents, :callers, :assignments

  		def initialize
  			reset
  		end

  		def reset
  			@queues = {}
  			@agents = {}
        @callers = {}
        @assignments = {}
        not_ready!
  		end

      # Queues

      def create_queue(queue_name)
        @queues[queue_name] = Queue.new(self, queue_name)
      end

      def find_queue(queue_name)
        @queues[queue_name]
      end

      # Agents

      def create_agent(agent_name)
        @agents[agent_name] = Agent.new(self, agent_name)
      end

      def find_agent(agent_name)
        @agents[agent_name]
      end

      def remove_agent(agent_name)
        @agents.delete(agent_name)
      end

      # Assignments

      def create_assignment(queue_name, agent_name)
        @assignments[[queue_name, agent_name]] = Assignment.new(self, queue_name, agent_name)
      end

      def find_assignment(queue_name, agent_name)
        @assignments[[queue_name, agent_name]]
      end

      def delete_assignment(queue_name, agent_name)
        @assignments.delete([queue_name, agent_name])
      end

      # Callers

      def create_caller(unique_id, queue_name)
        @callers[unique_id] = Caller.new(self, queue_name, unique_id)
      end

      def find_caller(unique_id)
        @callers[unique_id]
      end

      def delete_caller(unique_id)
        @callers.delete(unique_id)
      end

	  end
	end
end