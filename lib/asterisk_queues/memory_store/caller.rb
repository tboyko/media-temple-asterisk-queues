module AsteriskQueues
  module MemoryStore
    class Caller 
      include AsteriskQueues::Store::Caller
    
      attr_accessor :channel, :caller_id_num, :caller_id_name, :joined_at, :answered_at
      attr_reader :unique_id

      def initialize(store, queue_name, unique_id)
        @store      = store
        @queue_name = queue_name
        @unique_id  = unique_id
      end

      def queue
        @store.queues[@queue_name]
      end

      def agent
        @store.agents[@agent_name] if @agent_name
      end

      def set_agent(agent_name)
        @agent_name = agent_name
      end

      def save
      end
      
    end
  end
end