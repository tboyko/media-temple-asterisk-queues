module AsteriskQueues
  module MemoryStore
    class Agent

      attr_reader :name

      def initialize(store, name)
        @name     = name
        @store    = store
      end

      def callers
        @store.callers.values.select { |c| c.agent.equal? self }
      end

      def assignments
        @store.assignments.values.select { |a| a.agent.equal? self }
      end

    end
  end
end