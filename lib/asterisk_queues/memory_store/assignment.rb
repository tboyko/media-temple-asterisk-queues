module AsteriskQueues
  module MemoryStore
    class Assignment
      include AsteriskQueues::Store::Assignment

      attr_accessor :last_call, :status, :paused

      def initialize(store, queue_name, agent_name)
        @store = store
        @queue_name = queue_name
        @agent_name = agent_name
      end

      def queue
        @store.queues[@queue_name]
      end

      def agent
        @store.agents[@agent_name]
      end

      def save
      end

    end
  end
end
