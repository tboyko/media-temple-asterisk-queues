module AsteriskQueues
  class Spinup
  
    def self.run(stream, store, log)
      store.reset

      # Queue Status

      resp = stream.send_action('QueueStatus') 
      resp.events.each do |event|
        case event.name
        when 'QueueParams'
          # headers={"Queue"=>"3017", "Max"=>"0", "Strategy"=>"rrmemory", "Calls"=>"0", "Holdtime"=>"0", "TalkTime"=>"0", "Completed"=>"0", "Abandoned"=>"0", "ServiceLevel"=>"60", "ServicelevelPerf"=>"0.0", "Weight"=>"0", "ActionID"=>"3e083ba4-1881-4f05-8fd8-011ca5f561c1"}
          queue = store.create_queue(event.headers['Queue'])
          
          queue.estimated_hold_time = event.headers['Holdtime'].to_i
          queue.save
        when 'QueueMember'
          # headers={"Queue"=>"3016", "Name"=>"SIP/2362", "Location"=>"SIP/2362", "Membership"=>"dynamic", "Penalty"=>"0", "CallsTaken"=>"3", "LastCall"=>"1373495407", "Status"=>"2", "Paused"=>"0", "ActionID"=>"3e083ba4-1881-4f05-8fd8-011ca5f561c1"}
          queue = store.find_queue(event.headers['Queue'])
          agent = store.find_agent(event.headers['Name']) || store.create_agent(event.headers['Name'])
          qa    = store.create_assignment(queue.name, agent.name)

          qa.last_call = event.headers['LastCall'] != "0" ? Time.at(event.headers['LastCall'].to_i) : nil
          qa.status    = event.headers['Status'].to_i
          qa.paused    = event.headers['Paused'] == "1"
          qa.save 
        when 'QueueEntry'
          # headers={"Queue"=>"3009", "Position"=>"1", "Channel"=>"Skype/contact-mt-ad12ef78", "Uniqueid"=>"1373496315.69675", "CallerIDNum"=>"amartindf", "CallerIDName"=>"[BILL]Abelardo Mart\xC3\xADn", "ConnectedLineNum"=>"unknown", "ConnectedLineName"=>"unknown", "Wait"=>"14", "ActionID"=>"f2971d53-2b5f-48ac-9aee-375bf9aeff88"}
          caller  = store.create_caller(event.headers['Uniqueid'], event.headers['Queue'])
          caller.channel        = event.headers['Channel']
          caller.caller_id_num  = event.headers['CallerIDNum']
          caller.caller_id_name = event.headers['CallerIDName']
          caller.joined_at      = Time.now - event.headers['Wait'].to_i
          caller.save
        end
      end

      # Ongoing Queue Call Status

      resp = stream.send_action('CoreShowChannels')
      answered_queue_call_events = resp.events.select { |e| e.headers['Application'] == 'Queue' && !e.headers['BridgedChannel'].empty? }
      answered_queue_call_events.each do |event|
        queue   = store.find_queue(event.headers['Extension'])

        caller  = store.create_caller(event.headers['UniqueID'], queue.name)
        caller.channel        = event.headers['Channel']
        caller.caller_id_num  = event.headers['CallerIDnum']
        caller.caller_id_name = event.headers['CallerIDname']
        caller.answered_at    = Time.now - Duration.new(event.headers['Duration']).to_seconds
        caller.save

        agent_name  = Helper::bridged_channel_to_agent_name(event.headers['BridgedChannel'])
        agent       = store.find_agent(agent_name) || store.create_agent(agent_name)

        caller.set_agent(agent.name)
      end

      log.info('SPINUP') { 'Complete' }
    end

  end
end
