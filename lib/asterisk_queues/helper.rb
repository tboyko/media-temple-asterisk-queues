module AsteriskQueues
	module Helper
		def self.bridged_channel_to_agent_name(channel)
      match = channel.match(/^(SIP\/[^\-]+)/)
      match ? match[1] : nil
    end
  end
end