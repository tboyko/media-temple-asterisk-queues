class AsteriskQueues::Callbacks
  attr_accessor :complete, :join, :spinup

  def initialize
    self.complete = Proc.new { |queue, caller| }
    self.join     = Proc.new { |queue, caller| }
    self.spinup   = Proc.new { }
  end

end