require 'ruby_ami'

module AsteriskQueues
  class Client
    include Celluloid
    trap_exit :stream_exited

    attr_reader :store

    def initialize(opts)
      [:username, :password, :host].each do |req_opt|
        unless opts[req_opt] && !opts[req_opt].empty?
          raise ArgumentError, %{"#{req_opt}" must be provided}
        end
      end

      @host       = opts[:host]
      @port       = opts[:port] || 5038
      @username   = opts[:username]
      @password   = opts[:password]
      @timeout    = opts[:timeout] || 10
      @log        = opts[:logger] || ::Logger.new(nil)

      @callbacks  = opts[:callbacks] || AsteriskQueues::Callbacks.new

      @event_handler = EventHandler.new @callbacks, @log

      @store_class  = opts[:store] || MemoryStore::Store
    end

    def connect
      @store = @store_class.new

      @stream = RubyAMI::Stream.new_link  @host,
                                          @port,
                                          @username,
                                          @password,
                                          ->(e) { handle_event(e) },
                                          ::Logger.new(nil),
                                          @timeout

      @log.info('CORE') { "Connecting to Asterisk" }
      @stream.async.run

      # wait until we're connected. if @stream crashes, we return here and stream_exited starts us over.
      begin
        unless @stream.started?
          @log.warn('CORE') { "Connection failed" }
          return
        end
      rescue Celluloid::DeadActorError
        @log.warn('CORE') { "Encountered a dead actor after trying to connect" }
        return
      end

      @log.info('CORE') { "Connected" }
      Spinup.run @stream, @store, @log
      @store.ready!

      begin
        @callbacks.spinup.call
      rescue Exception => e
        @log.error('CORE') { "Spinup callback raised exception #{e.class}: #{e.message}. Terminating stream." }
        sleep 10
        @stream.terminate
      end
    end

    private

    def handle_event(event)
      @event_handler.handle event, @stream, @store
    end

    def stream_exited(actor, reason)
      reason_string = reason ? "#{reason.class}: #{reason.message}" : "none provided"

      @log.warn('CORE') { "Stream #{actor.inspect} exited. Reason: #{reason_string}. Rebuilding." }
      connect
    end

  end
end