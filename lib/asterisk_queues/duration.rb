module AsteriskQueues
  class Duration
  
    def initialize(duration)
      @duration = duration
    end

    def to_seconds
      m = @duration.match(/\A(?<hh>[^:]{2}):(?<mm>[^:]{2}):(?<ss>[^:]{2})\z/)
      m[:hh].to_i * 60 * 60 + m[:mm].to_i * 60 + m[:ss].to_i
    end

  end
end