module AsteriskQueues
  class EventHandler

    def initialize(callbacks, log)
      @callbacks  = callbacks
      @log        = log
    end

    def handle(event, stream, store)
      unless store.ready?
        @log.debug('EVENTHANDLER') { "Ignoring event because store is not ready. Event: #{event.inspect}" }
        return
      end

      if event.respond_to?('name') && self.respond_to?(event.name.underscore)
        @log.debug('EVENTHANDLER') { "Received event, sending to handler. Event: #{event.inspect}" }
        self.send(event.name.underscore, event.headers, stream, store)
      elsif event.respond_to? 'name'
        @log.debug('EVENTHANDLER') { "Received event with no handler. Event: #{event.inspect}" }
      end
    end

    def agent_called(headers, stream, store)
      queue_name  = headers['Queue']
      agent_name  = headers['AgentName']
      unique_id   = headers['Uniqueid']

      assignment = store.find_assignment(queue_name, agent_name)
      if assignment
        assignment.add_ringing_caller(unique_id)
      end

      @log.info('EVENTHANDLER') { "Queue #{queue_name}, agent #{agent_name}, caller #{unique_id} is ringing"}
    end

    def agent_complete(headers, stream, store)
      # destroy caller object, remove from queue and agent lists
      queue_name  = headers['Queue']
      agent_name  = headers['MemberName']
      unique_id   = headers['Uniqueid']

      queue  = store.find_queue(queue_name)
      caller = store.find_caller(unique_id)
      @callbacks.complete.call(queue, caller)

      store.delete_caller( unique_id )
      assignment = store.find_assignment(queue_name, agent_name)
      if assignment
        assignment.last_call = Time.now
        assignment.save
      end

      @log.info('EVENTHANDLER') { "Queue #{queue_name}, agent #{agent_name}, disconnected caller #{unique_id}" }
    end

    def agent_connect(headers, stream, store)
      queue_name  = headers['Queue']
      agent_name  = headers['MemberName']
      unique_id   = headers['Uniqueid']

      caller = store.find_caller(unique_id)
      caller.set_agent(agent_name)
      caller.answered_at = Time.now
      caller.save

      # indirectly trigger an update of estimated_hold_time

      stream.async.send_action( 'QueueSummary', { Queue: queue_name } )

      @log.info('EVENTHANDLER') { "Queue #{queue_name}, agent #{agent_name}, connected caller #{unique_id}" }
    end

    def queue_summary(headers, stream, store)
      queue_name  = headers['Queue']
      hold_time   = headers['HoldTime']

      queue                     = store.find_queue(queue_name)
      queue.estimated_hold_time = hold_time
      queue.save
    end

    def join(headers, stream, store)
      queue_name = headers['Queue']
      unique_id = headers['Uniqueid']

      queue   = store.find_queue(queue_name)
      caller  = store.create_caller(unique_id, queue_name)

      caller.channel        = headers['Channel']
      caller.caller_id_name = headers['CallerIDName']
      caller.caller_id_num  = headers['CallerIDNum']
      caller.joined_at      = Time.now
      caller.save

      @callbacks.join.call(queue, caller)

      @log.info('EVENTHANDLER') { "Queue #{queue_name}, joined caller #{unique_id}" }
    end

    def leave(headers, stream, store)

      #
      # This event happens:
      #
      # 1. after a QueueCallerAbandon (or other call), or
      # 2. before an AgentConnect
      #
      # If it's before an AgentConnect, we want the AgentConnect to 'handle' the record update,
      # so we wait a second to see if that happens. Otherwise, we need to remove the call from
      # the queue.
      #

      Thread.new {
        queue_name  = headers['Queue']
        unique_id   = headers['Uniqueid']

        sleep 1

        caller = store.find_caller(unique_id)

        if caller && caller.answered_at.nil?
          queue = store.find_queue(queue_name)
          @callbacks.complete.call(queue, caller)
          store.delete_caller(unique_id)
          @log.info('EVENTHANDLER') { "Queue #{queue_name}, caller #{unique_id}, left queue"}
        end
      }
    end

    def queue_member_added(headers, stream, store)
      queue_name  = headers['Queue']
      agent_name  = headers['MemberName']

      agent       = store.find_agent(agent_name) || store.create_agent(agent_name)
      assignment  = store.create_assignment(queue_name, agent_name)
      assignment.status    = headers['Status'].to_i
      assignment.paused    = headers['Paused'] == "1"
      assignment.paused_at = (assignment.paused ? Time.now : nil)
      assignment.save

      @log.info('EVENTHANDLER') { "Queue #{queue_name}, added agent #{agent_name}" }
    end

    def queue_member_paused(headers, stream, store)
      queue_name = headers['Queue']
      agent_name = headers['MemberName']
      new_paused = headers['Paused'] == "1"

      assignment = store.find_assignment(queue_name, agent_name)
      unless assignment.paused == new_paused
        assignment.paused    = headers['Paused'] == "1"
        assignment.paused_at = (assignment.paused ? Time.now : nil)
        assignment.save
        @log.info('EVENTHANDLER') { "Queue #{queue_name}, #{assignment.paused ? '' : 'un' }paused agent #{agent_name}" }
      end
    end

    def queue_member_removed(headers, stream, store)
      queue_name  = headers['Queue']
      agent_name  = headers['MemberName']

      store.delete_assignment(queue_name, agent_name)
      agent = store.find_agent(agent_name)
      store.remove_agent(agent.name) if agent.assignments.count == 0

      @log.info('EVENTHANDLER') { "Queue #{queue_name}, removed agent #{agent_name}" }
    end

    def queue_member_status(headers, stream, store)
      queue_name = headers['Queue']
      agent_name = headers['MemberName']
      new_status = headers['Status'].to_i

      assignment = store.find_assignment(queue_name, agent_name)
      unless assignment.status == new_status
        assignment.status = new_status
        assignment.save

        unless [6,7].include?(new_status)
          # remove callers ringing this assignment if assignment status is no longer "ringing"
          assignment.clear_ringing_callers
        end

        status = Store::Assignment::STATUS[assignment.status]
        @log.info('EVENTHANDLER') { "Queue #{queue_name}, agent #{agent_name}, status changed to #{status}" }
      end
    end

  end
end
