# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'asterisk_queues/version'

Gem::Specification.new do |spec|
  spec.name          = "asterisk_queues"
  spec.version       = AsteriskQueues::VERSION
  spec.authors       = ["Taylor Boyko"]
  spec.email         = ["taylor@wrprojects.com"]
  spec.description   = %q{}
  spec.summary       = %q{}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency             "ruby_ami",       "~> 2.4"
  spec.add_dependency             "celluloid",      "~> 0.14"
  spec.add_development_dependency "rspec",          "~> 2.0"
  spec.add_development_dependency "bundler",        "~> 1.3"
  spec.add_development_dependency "rake"
end
