require 'spec_helper'

module AsteriskQueues
  module MemoryStore
    describe Queue do
      subject { Queue.new 'test_queue' }

      it { should respond_to(:name) }

      before do
        @agent_name = 'SIP/1234'
        @agent = subject.find_or_create_agent(@agent_name)
        @caller_unique_id = '12312343241235.123123'
        @caller = subject.find_or_create_caller(@caller_unique_id)
      end

      it 'should return a hash of agents' do
        subject.agents.should be_a_kind_of(Hash)
        subject.agents.count.should equal(1)
      end

      it 'should return a hash of callers' do
        subject.callers.should be_a_kind_of(Hash)
        subject.callers.count.should equal(1)
      end

      describe '#find_or_create_agent' do
        it 'should return an agent when created' do
          @agent.should be_a_kind_of(Agent)
        end

        it 'should find pre-existing agents' do
          agent2 = subject.find_or_create_agent(@agent_name)
          agent2.should equal(@agent)
        end
      end

      describe '#remove_agent' do
        it 'should remove an existing agent' do
          subject.remove_agent(@agent_name)
          subject.agents.count.should equal(0)
        end
      end

      describe '#find_caller' do
        it 'should find an existing caller' do
          subject.find_caller(@caller_unique_id).should be_a_kind_of(Caller)
        end
      end

      describe '#find_or_create_caller' do
        it 'should find an existing caller' do
          subject.find_or_create_caller(@caller_unique_id).should equal(@caller)
        end
      end

      describe '#remove_caller' do
        it 'should remove an existing caller' do
          subject.remove_caller(@caller_unique_id)
          subject.callers.count.should equal(0)
        end
      end

    end
  end
end