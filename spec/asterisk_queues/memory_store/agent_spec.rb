require 'spec_helper'

module AsteriskQueues
  module MemoryStore
    describe Agent do

      subject { Agent.new 'SIP/1234' }

      before do
        @caller_unique_id = '12343124142.12121'
        @caller = Caller.new @caller_unique_id
        subject.add_caller(@caller)
      end

      it 'should be able to add a caller' do
      end

      it { should respond_to(:name) }

      it { should respond_to(:last_call) }

      it { should respond_to(:status) }

      it { should respond_to(:paused) }

      it 'should return a hash of callers' do
        callers = subject.callers
        callers.should be_a_kind_of(Hash)
        callers.count.should equal(1)
      end

      it 'should provide a STATUS hash' do
        subject.class::STATUS.should be_a_kind_of(Hash)
      end

      it 'should be able to remove a caller' do
        subject.remove_caller(@caller_unique_id)
        subject.callers.count.should equal(0)
      end

    end
  end
end