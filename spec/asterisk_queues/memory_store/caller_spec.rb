require 'spec_helper'

module AsteriskQueues
  module MemoryStore
    describe Caller do

      subject { Caller.new '123121231231.1111' }

      it { should respond_to(:channel) }

      it { should respond_to(:unique_id) }

      it { should respond_to(:caller_id_num) }

      it { should respond_to(:caller_id_name) }

      it { should respond_to(:joined_at) }

      it { should respond_to(:answered_at) }

      describe '#set_agent' do
        before do
          @agent = Agent.new 'SIP/1234'
          subject.set_agent(@agent)
        end

        it 'should be able to retrieve a set agent' do
          subject.agent.should equal(@agent)
        end
      end

    end
  end
end