require 'spec_helper'

module AsteriskQueues
  module MemoryStore
    describe Store do
      before do 
        @queue_names = %w{first second third first}
        @queue_names.each { |n| subject.find_or_create_queue(n) }
      end

      describe '#find_or_create_queue' do
        it 'returns a queue when created' do
          subject.find_or_create_queue('new').should be_a_kind_of(Queue)
        end

        it 'finds a queue' do
          name = 'some queue'
          queue1 = subject.find_or_create_queue(name)
          queue2 = subject.find_or_create_queue(name)

          queue1.should equal(queue2)
        end
      end

      describe '#queues' do

        it 'returns queues' do
          queues = subject.queues
          queues.should be_a_kind_of(Hash)
          queues.count.should equal(3)
          queues.first.last.should be_a_kind_of(Queue)
        end
      end

      describe '#reset' do
        before do
          subject.reset
        end

        it 'should have no queues after a reset' do
          subject.queues.count.should equal(0)
        end
      end

    end
  end
end
