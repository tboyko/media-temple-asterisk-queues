# AsteriskQueues

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'asterisk_queues'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install asterisk_queues

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request


Store
	find_or_create_queue
Queue
	find_or_create_agent
	remove_agent
	find_caller(unique_id)
	find_or_create_caller
	remove_caller
Agent
	add_caller(caller)
	remove_caller(unique_id) # only to be called by Queue class!
Caller
	set_agent(agent) # only to be called by Agent class!


Caller joins queue
	find_or_create_caller
caller leaves queue
	remove_caller
caller connects to agent
	add_caller
caller hangs up with agent
	queue.remove_caller

make store.queues a copy

what if queue.remove_agent on existing call, what happens to call when cleaned up?

what about a transfer?